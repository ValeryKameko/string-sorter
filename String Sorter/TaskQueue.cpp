#include "TaskQueue.h"
#include <Windows.h>

constexpr int SPIN_COUNT = 20;
constexpr int MAX_SEMAPHORE_COUNT = 1024;

template <class T>
TaskQueue<T>::TaskQueue()
{
	InitializeCriticalSectionAndSpinCount(&CriticalSection, SPIN_COUNT);
	Semaphore = CreateSemaphore(
		nullptr, 0, MAX_SEMAPHORE_COUNT, nullptr);
}

template <class T>
void TaskQueue<T>::EnqueueTask(T* task)
{
	EnterCriticalSection(&CriticalSection);

	Tasks.push(task);
	ReleaseSemaphore(Semaphore, 1, nullptr);
	
	LeaveCriticalSection(&CriticalSection);
}

template <class T>
T* TaskQueue<T>::DequeueTask()
{
	WaitForSingleObject(Semaphore, INFINITE);
	EnterCriticalSection(&CriticalSection);
	
	T* task = Tasks.pop();

	LeaveCriticalSection(&CriticalSection);
	return task;
}
