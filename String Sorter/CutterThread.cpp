﻿#include "CutterThread.h"
#include "Line.h"

CutterThread::CutterThread(
	const Region& cuttingRegion,
	SIZE_T piecesCount,
	TaskQueue<Region>& regionTaskQueue)
	: RegionTaskQueue(regionTaskQueue), CuttingRegion(cuttingRegion), PiecesCount(piecesCount)
{

}


SIZE_T CutterThread::CalculateLinesCount(const Region& cuttingRegion)
{
	SIZE_T linesCount = 0;
	LPCH startAddress = LPCH(cuttingRegion.start);
	LPCH endAddress = LPCH(cuttingRegion.end);
	for (LPCH address = startAddress; address < endAddress;)
	{
		Line line = Line::CalculateLine(address, endAddress);
		address += line.size + 2;
		linesCount++;
	}
	return linesCount;
}

DWORD CutterThread::Run()
{
	SIZE_T linesCount = CalculateLinesCount(CuttingRegion);

	SIZE_T lineBatch = linesCount / PiecesCount;
	SIZE_T lineRemainder = linesCount - lineBatch * PiecesCount;
	SIZE_T currentBatch = 0;

	LPVOID startRegion = CuttingRegion.start;
	LPVOID endRegion = CuttingRegion.start;

	for (LPCH address = CuttingRegion.start; address < CuttingRegion.end;)
	{
		Line line = Line::CalculateLine(address, CuttingRegion.end);
		endRegion = address + line.size;
		address += line.size + 2;
		currentBatch++;
		if (currentBatch == (lineBatch + (lineRemainder > 0)))
		{
			RegionTaskQueue.EnqueueTask(new Region(startRegion, endRegion));
			startRegion = address;
			currentBatch = 0;
			if (lineRemainder)
				lineRemainder--;
		}
	}
	RegionTaskQueue.EnqueueTask(TaskQueue<Region>::POISON_PILL);
}

CutterThread::~CutterThread()
{
}
