#include "ThreadPool.h"
#include <Windows.h>
#include "Thread.h"

ThreadPool::ThreadPool()	
{
}

ThreadPool::~ThreadPool()
{
}

void ThreadPool::Start()
{
	for (const std::shared_ptr<Thread>& thread : threads)
	{
		thread->Start();
	}
}

void ThreadPool::Stop()
{
	for (const std::shared_ptr<Thread>& thread : threads)
	{
		thread->Stop();
	}
}

void ThreadPool::Complete()
{
	std::vector<HANDLE> handles;
	handles.reserve(threads.size());
	for (const std::shared_ptr<Thread>& thread : threads)
	{
		handles.push_back(thread->GetHandle());
	}
	WaitForMultipleObjects(threads.size(), handles.data(), TRUE, INFINITE);
}

void ThreadPool::AddThread(const std::shared_ptr<Thread>& thread)
{
	threads.push_back(thread);
}