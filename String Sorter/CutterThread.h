﻿#pragma once
#include "Thread.h"
#include "TaskQueue.h"
#include "Region.h"

class CutterThread final : public Thread
{
public:
	explicit CutterThread(
		const Region& cuttingRegion,
		SIZE_T piecesCount,
		TaskQueue<Region>& regionTaskQueue);

	DWORD Run() override;
	~CutterThread() override;

private:
	TaskQueue<Region>& RegionTaskQueue;
	const Region& CuttingRegion;
	SIZE_T PiecesCount;

	static SIZE_T CalculateLinesCount(const Region& cuttingRegion);
};
