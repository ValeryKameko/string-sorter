﻿#include <iostream>
#include "MappedFile.h"
#include "ThreadPool.h"
#include "Line.h"
#include "SortMerger.h"
#include "Region.h"
#include "SorterThread.h"
#include <string>

MappedFile* GetSortingFile()
{
	std::wstring sortingFileName;
	std::cout << "Enter sorting file: ";
	std::getline(std::wcin, sortingFileName);

	MappedFile* sortingFile = new MappedFile{sortingFileName.c_str(), GENERIC_READ | GENERIC_WRITE, OPEN_ALWAYS};
	return sortingFile;
}


MappedFile* GetOutFile(SIZE_T size)
{
	std::wstring outFileName;
	std::cout << "Enter output file: ";
	std::getline(std::wcin, outFileName);

	MappedFile* outFile = new MappedFile{outFileName.c_str(), GENERIC_READ | GENERIC_WRITE, CREATE_ALWAYS, size};
	return outFile;
}

void SortFile(const MappedFile& sortingFile, const MappedFile& outFile, unsigned int threadCount)
{
	SIZE_T linesCount = 0;

	LPCH startAddress = LPCH(sortingFile.GetMappingAddress());
	LPCH endAddress = startAddress + sortingFile.GetSize();
	for (LPCH address = startAddress; address < endAddress;)
	{
		Line line = Line::CalculateLine(address, endAddress);
		address += line.size + 2;
		linesCount++;
	}

	ThreadPool threadPool;

	std::vector<Region> regions;
	
	SIZE_T lineBatch = linesCount / threadCount;
	SIZE_T lineRemainder = linesCount - lineBatch * threadCount;
	SIZE_T currentBatch = 0;
	
	LPVOID startRegion = startAddress;
	LPVOID endRegion = startAddress;
	
	for (LPCH address = startAddress; address < endAddress;)
	{
		Line line = Line::CalculateLine(address, endAddress);
		endRegion = address + line.size;
		address += line.size + 2;
		currentBatch++;
		if (currentBatch == (lineBatch + (lineRemainder > 0)))
		{
			regions.emplace_back(startRegion, endRegion);
			startRegion = address;
			currentBatch = 0;
			if (lineRemainder)
				lineRemainder--;
		}
	}
	std::vector<std::shared_ptr<SorterThread>> sorters;
	for (const Region& region : regions)
	{
		std::shared_ptr<SorterThread> thread{ new SorterThread{region.start, region.end} };
		threadPool.AddThread(thread);
		sorters.push_back(thread);
	}

	threadPool.Start();
	threadPool.Complete();

	std::vector<Region> tempRegions;
	for (const std::shared_ptr<SorterThread>& thread : sorters)
	{
		const MappedFile* tmpFile = thread->GetTempFile();
		LPCH tmpStart = LPCH(tmpFile->GetMappingAddress());
		LPCH tmpEnd = tmpStart + tmpFile->GetSize();
		tempRegions.emplace_back(tmpStart, tmpEnd);
	}

	LPCH outStart = LPCH(outFile.GetMappingAddress());
	LPCH outEnd = outStart + outFile.GetSize();
	SortMerger::Merge(Region(outStart, outEnd), tempRegions);
}

unsigned long GetThreadCount()
{
	unsigned int threadCount;
	std::cout << "Enter thread count: ";
	std::cin >> threadCount;
	return threadCount;
}

int main()
{
	MappedFile* sortingFile = GetSortingFile();
	MappedFile* outFile = GetOutFile(sortingFile->GetSize());
	unsigned long threadCount = GetThreadCount();

	LARGE_INTEGER startInterval;
	QueryPerformanceCounter(&startInterval);
	
	SortFile(*sortingFile, *outFile, threadCount);

	LARGE_INTEGER endInterval;
	QueryPerformanceCounter(&endInterval);

	LARGE_INTEGER frequency;
	QueryPerformanceFrequency(&frequency);

	double seconds = (endInterval.QuadPart - startInterval.QuadPart) / double(frequency.QuadPart);
	
	std::clog << "Sorting finished in " << seconds << "\n";
	
	delete sortingFile;
	delete outFile;
	std::cout << "File sorted\n";
	return 0;
}
