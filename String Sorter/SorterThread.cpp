#include "SorterThread.h"
#include <vector>
#include "MappedFile.h"


void SorterThread::SortLines(std::vector<Line>& lines)
{
	std::vector<Line> temp = lines;
	SortLinesRange(lines.begin(), lines.end(), temp.begin(), temp.end());
	swap(lines, temp);
}

void SorterThread::SortLinesRange(
	std::vector<Line>::iterator rangeStart, std::vector<Line>::iterator rangeEnd,
	std::vector<Line>::iterator tempStart, std::vector<Line>::iterator tempEnd)
{
	auto rangeSize = std::distance(rangeStart, rangeEnd)	;
	if (rangeSize <= 1)
	{
		std::copy(rangeStart, rangeEnd, tempStart);
		return;
	}
	const std::vector<Line>::iterator
		leftStart = rangeStart,
		leftEnd = rangeStart + rangeSize / 2,
		rightStart = rangeStart + rangeSize / 2,
		rightEnd = rangeEnd,
		tempLeftStart = tempStart,
		tempLeftEnd = tempStart + rangeSize / 2,
		tempRightStart = tempStart + rangeSize / 2,
		tempRightEnd = tempEnd;

	SortLinesRange(tempLeftStart, tempLeftEnd, leftStart, leftEnd);
	SortLinesRange(tempRightStart, tempRightEnd, rightStart, rightEnd);

	std::vector<Line>::iterator
		leftIt = leftStart,
		rightIt = rightStart;

	for (std::vector<Line>::iterator tempIt = tempStart; tempIt != tempEnd; ++tempIt)
	{
		if (leftIt == leftEnd || ((rightIt != rightEnd) && (Line::CompareLines(*leftIt, *rightIt) > 0))) {
			*tempIt = *rightIt;
			++rightIt;
		}
		else {
			*tempIt = *leftIt;
			++leftIt;
		}
	}
}

LPCWSTR SorterThread::GenerateTempFileName()
{
	static LPCWSTR tempPrefix = L"sst";
	LPWSTR tempPath = new WCHAR[MAX_PATH + 1];
	GetTempPath(MAX_PATH, tempPath);

	LPWSTR tempFileName = new WCHAR[MAX_PATH + 1];
	GetTempFileNameW(tempPath, tempPrefix, 0, tempFileName);

	delete[] tempPath;
	return tempFileName;
}

SorterThread::SorterThread(LPVOID startAddress, LPVOID endAddress)
{
	this->StartAddress = startAddress;
	this->EndAddress = endAddress;
	SIZE_T size = LPBYTE(endAddress) - startAddress;

	LPCWSTR tempFileName = GenerateTempFileName();
	tempFile.reset(new MappedFile(tempFileName, GENERIC_READ | GENERIC_WRITE, CREATE_ALWAYS, size));
	delete[] tempFileName;
}

DWORD SorterThread::Run()
{
	std::vector<Line> lines = Line::CalculateLines(StartAddress, EndAddress);
	SortLines(lines);

	LPCH address = LPCH(tempFile->GetMappingAddress());
	LPCH endAddress = address + tempFile->GetSize();
	for (Line line : lines)
	{
		memcpy(address, line.start, line.size);
		address += line.size;
		if (address < endAddress)
		{
			*address = '\r';
			address++;
		} else
			break;
		if (address < endAddress)
		{
			*address = '\n';
			address++;
		} else
			break;
	}
	return 0;
}

const MappedFile* SorterThread::GetTempFile()
{
	return tempFile.get();
}
