﻿#pragma once

#include<Windows.h>

struct Region final
{
	Region(LPVOID start, LPVOID end);

	LPVOID end;
	LPVOID start;
	SIZE_T size;
};
