#pragma once

#include <queue>
#include <Windows.h>

template<class T>
class TaskQueue final
{
public:
	TaskQueue();

	void EnqueueTask(T* task);
	T* DequeueTask();

	static T* const POISON_PILL = nullptr;
private:
	std::queue<T*> Tasks;

	CRITICAL_SECTION CriticalSection;
	HANDLE Semaphore;
};
