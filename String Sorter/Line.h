#pragma once
#include <Windows.h>
#include <iosfwd>
#include <vector>

struct Line
{
	LPCH start;
	SIZE_T size;
	Line(LPCH start, SIZE_T size);
	
	static INT CompareLines(const Line& line1, const Line& line2);
	static Line CalculateLine(LPVOID startAddress, LPVOID endAddress);
	static std::vector<Line> CalculateLines(LPVOID startAddress, LPVOID endAddress);
};
