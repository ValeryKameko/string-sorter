#include "Thread.h"


Thread::~Thread()
{
	CloseHandle(ThreadHandle);
}

void Thread::Start() const
{
	ResumeThread(ThreadHandle);
}

void Thread::Stop() const
{
	SuspendThread(ThreadHandle);
}

DWORD Thread::GetReturnValue() const
{
	return ReturnValue;
}
	
HANDLE Thread::GetHandle() const
{
	return ThreadHandle;
}

Thread::Thread(SIZE_T threadStackSize)
{
	const DWORD creationFlags = CREATE_SUSPENDED	;
	ThreadHandle = CreateThread(nullptr, threadStackSize, ThreadProc, this, creationFlags, nullptr);
	ReturnValue = 0;
}

DWORD WINAPI Thread::ThreadProc(LPVOID param)
{
	Thread* thread = reinterpret_cast<Thread*>(param);
	thread->ReturnValue = thread->Run();
	return thread->ReturnValue;
}
