#include <Windows.h>
#include "Line.h"
#include <vector>


Line::Line(LPCH start, SIZE_T size)
	: start(start), size(size)
{ }

INT Line::CompareLines(const Line& line1, const Line& line2)
{
	int cmpResult = strncmp(line1.start, line2.start, min(line1.size, line2.size));
	if (cmpResult != 0)
		return cmpResult;
	if (line1.size < line2.size)
		return -1;
	if (line1.size > line2.size)
		return +1;
	return 0;
}

Line Line::CalculateLine(LPVOID startAddress, LPVOID endAddress)
{
	if (startAddress >= endAddress)
		return { LPCH(startAddress), 0 };
	LPCH startLine = LPCH(startAddress);
	SIZE_T size = LPBYTE(endAddress) - startAddress;
	LPCH endLine = LPCH(memchr(startAddress, '\r', size));
	if (!endLine)
		endLine = LPCH(endAddress);
	return {startLine, SIZE_T(endLine - startLine)};
}

std::vector<Line> Line::CalculateLines(LPVOID startAddress, LPVOID endAddress)
{
	std::vector<Line> lines;

	bool wasEnd = true;
	SIZE_T size = 0;
	for (LPCH address = LPCH(startAddress); address < endAddress;)
	{
		Line line = CalculateLine(address, endAddress);
		lines.push_back(line);
		address += line.size + 2;
	}
	return lines;
}