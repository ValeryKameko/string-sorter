#pragma once

template<class T>
class TaskProcessor
{
public:
	virtual void Process(T* task) = 0;
};

