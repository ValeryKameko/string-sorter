#pragma once
#include "Thread.h"
#include <vector>
#include <memory>
#include "Line.h"

class MappedFile;

class SorterThread :
	public Thread
{
public:
	SorterThread(LPVOID startAddress, LPVOID endAddress);

	DWORD Run() override;

	const MappedFile* GetTempFile();

private:

	LPVOID StartAddress;
	LPVOID EndAddress;

	std::unique_ptr<MappedFile> tempFile;

	static void SortLines(std::vector<Line>& lines);
	static void SortLinesRange(
		std::vector<Line>::iterator rangeStart, std::vector<Line>::iterator rangeEnd,
		std::vector<Line>::iterator tempStart, std::vector<Line>::iterator tempEnd);
	static LPCWSTR GenerateTempFileName();
};

