﻿#pragma once

#include <windows.h>
#include <xstring>

class MappedFile final
{
public:
	MappedFile(LPCWSTR fileName, DWORD access, DWORD creation);
	MappedFile(LPCWSTR fileName, DWORD access, DWORD creation, SIZE_T size);
	~MappedFile();

	SIZE_T GetSize() const;
	LPVOID GetMappingAddress() const;

	void Flush() const;

private:
	HANDLE File;
	HANDLE FileMapping;
	LPVOID MappingAddress;
	SIZE_T Size;

	static HANDLE CreateFileW(LPCWSTR fileName, DWORD access, DWORD creation);
	static HANDLE CreateFileW(LPCWSTR fileName, DWORD access, DWORD creation, SIZE_T size);
	static HANDLE CreateFileMappingW(HANDLE file, DWORD access, SIZE_T size);
	static LPVOID MapViewOfFile(HANDLE fileMapping, DWORD access, SIZE_T size);
};