﻿#include "MappedFile.h"

MappedFile::MappedFile(LPCWSTR fileName, DWORD access, DWORD creation)
{
	File = CreateFileW(fileName, access, creation);

	LARGE_INTEGER FileSize;
	GetFileSizeEx(File, &FileSize);
	Size = FileSize.QuadPart;

	FileMapping = CreateFileMappingW(File, access, Size);

	MappingAddress = MapViewOfFile(FileMapping, access, Size);
}

MappedFile::MappedFile(LPCWSTR fileName, DWORD access, DWORD creation, SIZE_T size)
{
	File = CreateFileW(fileName, access, creation, size);

	Size = size;

	FileMapping = CreateFileMappingW(File, access, Size);

	MappingAddress = MapViewOfFile(FileMapping, access, Size);
}

void MappedFile::Flush() const
{
	::FlushViewOfFile(MappingAddress, Size);
}

MappedFile::~MappedFile()
{
	if (MappingAddress) {
		Flush();
		UnmapViewOfFile(MappingAddress);
	}
	if (FileMapping)
		CloseHandle(FileMapping);
	if (File)
		CloseHandle(File);
}

SIZE_T MappedFile::GetSize() const
{
	return Size;
}

LPVOID MappedFile::GetMappingAddress() const
{
	return MappingAddress;
}

HANDLE MappedFile::CreateFileW(LPCWSTR fileName, DWORD access, DWORD creation)
{
	return ::CreateFile(
		fileName,
		access,
		0,
		nullptr,
		creation,
		FILE_ATTRIBUTE_NORMAL,
		nullptr);
}

HANDLE MappedFile::CreateFileW(LPCWSTR fileName, DWORD access, DWORD creation, SIZE_T size)
{
	const HANDLE file = CreateFileW(fileName, access, creation);
	if (!file)
		return file;

	LARGE_INTEGER sizeInteger;
	sizeInteger.QuadPart = size;
	SetFilePointerEx(file, sizeInteger, nullptr, FILE_BEGIN);

	SetEndOfFile(file);
	return file;
}

HANDLE MappedFile::CreateFileMappingW(HANDLE file, DWORD access, SIZE_T size)
{
	DWORD fileMapProtect = 0;
	if ((access & GENERIC_READ) && (access & GENERIC_WRITE))
		fileMapProtect |= PAGE_READWRITE;
	else if (access & GENERIC_READ)
		fileMapProtect |= PAGE_READONLY;
	else if (access & GENERIC_WRITE)
		fileMapProtect |= PAGE_WRITECOPY;


	const HANDLE fileMapping = ::CreateFileMapping(
		file,
		nullptr,
		fileMapProtect,
		size & 0xffffffff00000000,
		size & 0x00000000ffffffff,
		nullptr);
	return fileMapping;
}

LPVOID MappedFile::MapViewOfFile(HANDLE fileMapping, DWORD access, SIZE_T size)
{
	DWORD mapViewAccess = 0;
	if ((access & GENERIC_READ) && (access & GENERIC_WRITE))
		mapViewAccess |= FILE_MAP_ALL_ACCESS;
	else if (access & GENERIC_READ)
		mapViewAccess |= FILE_MAP_READ;
	else if (access & GENERIC_WRITE)
		mapViewAccess |= FILE_MAP_WRITE;

	return ::MapViewOfFile(
		fileMapping,
		mapViewAccess,
		0,
		0,
		size);
}
