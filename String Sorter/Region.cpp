﻿#include "Region.h"

Region::Region(LPVOID start, LPVOID end)
{
	this->start = start;
	this->end = end;
	this->size = LPBYTE(end) - start;
}
