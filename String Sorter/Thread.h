#pragma once
#include <windows.h>

constexpr SIZE_T DEFAULT_THREAD_STACK_SIZE = 1024 * 1024;

class Thread
{
public:
	virtual ~Thread();

	virtual DWORD Run() = 0;

	void Start() const;
	void Stop() const;
	DWORD GetReturnValue() const;
	HANDLE GetHandle() const;
	
protected:
	Thread(SIZE_T threadStackSize = DEFAULT_THREAD_STACK_SIZE);

private:
	HANDLE ThreadHandle;
	DWORD ReturnValue;

	static DWORD WINAPI ThreadProc(LPVOID param);
};

