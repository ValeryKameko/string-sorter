#pragma once
#include <vector>
#include <memory>

class Thread;

class ThreadPool final
{
public:
	ThreadPool();
	~ThreadPool();

	void Start();
	void Stop();
	void Complete();

	void AddThread(const std::shared_ptr<Thread>& thread);

private:
	std::vector<std::shared_ptr<Thread>> threads;
};

