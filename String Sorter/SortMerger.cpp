#include "SortMerger.h"
#include "Line.h"


void SortMerger::Merge(const Region& region, const std::vector<Region>& regions)
{
	std::vector<Line> regionLines;
	regionLines.reserve(regions.size());
	for (const Region& region : regions)
		regionLines.push_back(Line::CalculateLine(region.start, region.end));

	LPCH address = LPCH(region.start);
	SIZE_T size = region.size;
	while (true)
	{
		int minIndex = -1;
		Line* minLine = nullptr;
		for (int i = 0; i < regionLines.size(); i++) 
		{
			Line& line = regionLines[i];	
			if (line.start < regions[i].end && (!minLine || Line::CompareLines(*minLine, line) > 0))
			{
				minLine = &line;
				minIndex = i;
			}
		}
		if (minIndex == -1)
			break;

		memcpy_s(address, size, minLine->start, minLine->size);
		address += minLine->size;
		size -= minLine->size;

		memcpy_s(address, size, "\r\n", min(size, 2));
		address += min(size, 2);
		size -= min(size, 2);
		
		LPCH const nextLineAddress = LPCH(regionLines[minIndex].start) + regionLines[minIndex].size + 2;
		Line nextLine = Line::CalculateLine(nextLineAddress, regions[minIndex].end);

		regionLines[minIndex] = nextLine;
	}
}
