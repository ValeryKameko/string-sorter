#pragma once
#include <Windows.h>
#include <vector>
#include "Region.h"

class SortMerger final
{
public:	
	static void Merge(const Region& region, const std::vector<Region>& regions);
};

