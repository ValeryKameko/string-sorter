import random
import string

DefaultSaveDirectory = '../data'

def generateRandomString(length: int) -> str:
    return ''.join(random.choice(string.ascii_letters) for i in range(length))

def generate(directory: string, lineCount: int, lengthRange: int) -> [str]:
    with open(f'{directory}/data_{lineCount}_{lengthRange[0]}-{lengthRange[1]}.txt', 'w+') as f:
        for i in range(lineCount):
            randomLength = random.randint(lengthRange[0], lengthRange[1])
            randomString = generateRandomString(randomLength)
            f.write(f'{randomString}\n')


def main() -> ():
    lineCount = int(input('Enter line count: '))
    lengthRange = [int(word) for word in input('Enter length range (for example: 1 2): ').split()]
    saveDirectory = input(f'Enter save directory (default: {DefaultSaveDirectory})')
    if saveDirectory == '':
        saveDirectory = DefaultSaveDirectory

    generate(saveDirectory, lineCount, lengthRange)

if __name__ == '__main__':
    main()